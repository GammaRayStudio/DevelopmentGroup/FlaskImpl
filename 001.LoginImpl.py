from flask import render_template, request, jsonify , session , redirect , url_for
from werkzeug.utils import secure_filename
from main import app , db , ALLOWED_EXTENSIONS
from model import UserInfo
from datetime import datetime
import os

@app.route('/')
@app.route('/login')
def home():
    userInfo = None
    isLogin , account , msg = getStatus()
    if isLogin :
        userInfo = db.session.query(UserInfo).filter_by(account=account).first()

    return render_template('login.html', login_status = isLogin , login_msg = msg , user_info = userInfo)

def getStatus():
    isLogin = False
    account = ''
    msg = ''
    if 'isLogin' in session:
        isLogin = session['isLogin']
        account = session['account']
        msg = session['loginMsg']
        if isLogin is False:
            session.pop('isLogin', None)
            session.pop('loginMsg' , None)
    return isLogin , account ,  msg

@app.route('/register' , methods=['POST'])
def register():
    if request.method == 'POST':
        name = request.form['name']
        account = request.form['account']
        password = request.form['password']
        photo = request.files['photo']

        root , extension = os.path.splitext(photo.filename)
        filename = account + extension

        if photo and allowed_file(photo.filename):
            photo.save(os.path.join(
                app.config['UPLOAD_FOLDER'], secure_filename(filename)))

        userInfo = UserInfo(name , account , password , filename)
        # 資料庫
        db.session.add(userInfo)
        db.session.commit()

        # 自動登入
        session['account'] = account
        session['isLogin'] = True
        session['loginMsg'] = "登入成功"

        return redirect(url_for('home'))

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS



@app.route('/login', methods=['POST'])
def login():
    if request.method == 'POST':
        account = request.form['account']
        password = request.form['password']

        userInfo = db.session.query(UserInfo).filter_by(account=account).first()

        print(type(userInfo))
        print(userInfo)
        if userInfo != None :
            if userInfo.account == account and userInfo.password == password :
                session['account'] = account
                session['isLogin'] = True
                session['loginMsg'] = "登入成功"
            else :
                session['account'] = ''
                session['isLogin'] = False
                session['loginMsg'] = "帳號或密碼錯誤"
        else :
            session['account'] = ''
            session['isLogin'] = False
            session['loginMsg'] = "帳號不存在"
        return redirect(url_for('home'))

@app.route('/logout' , methods=['GET'])
def logout():
    session.pop('account', None)
    session.pop('isLogin', None)
    session.pop('loginMsg' , None)
    return redirect(url_for('home'))

@app.route('/change' , methods = ['POST'])
def change():
    old_pw = request.form['oldPassword']
    new_pw = request.form['newPassword']
    chk_pw = request.form['chkPassword']

    account = session['account']
    userInfo = db.session.query(UserInfo).filter_by(account=account).first()

    isOldPwCorrect = False
    isNewPwCorrect = False

    # 驗證舊密碼
    if userInfo.password == old_pw : 
        isOldPwCorrect = True
    
    # 驗證新密碼
    if isOldPwCorrect :
        if new_pw == chk_pw:
            isNewPwCorrect = True

    # 確認修改
    if isNewPwCorrect :
        userInfo.password = new_pw
        db.session.commit()

    return redirect(url_for('home'))

if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)