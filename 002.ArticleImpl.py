from crypt import methods
import json
from flask import render_template, request, jsonify , session , redirect , url_for
from werkzeug.utils import secure_filename
from main import app , db , ALLOWED_EXTENSIONS
from model import Article 
from datetime import datetime
import os

@app.route('/')
@app.route('/article' , methods = ['GET'])
def article():
    lstRecord = Article.query.all()
    lstArticle = []
    for record in lstRecord :
        article = record.as_dict()
        print(article)
        article['date'] = article['date'].strftime("%Y-%m-%d %H:%M:%S")
        lstArticle.append(article)
    return render_template('article.html' , lstArticle = lstArticle)

@app.route('/add' , methods = ['POST'])
def add():
    article = Article(
        request.form['title'] , request.form['content'] ,
        'DevAuth' , datetime.now()
    )
    db.session.add(article)
    db.session.commit()
    return redirect(url_for('article'))

@app.route('/edit' , methods = ['POST'])
def edit():
    row_id = request.form['id']
    article = db.session.query(Article).filter_by(id=row_id).first()
    article.title = request.form['title']
    article.content = request.form['content']
    article.author = 'DevAuth'
    article.date = datetime.now()
    db.session.add(article)
    db.session.commit()
    return redirect(url_for('article'))

@app.route('/delete' , methods = ['POST'])
def delete():
    row_id = request.form['id']
    article = db.session.query(Article).filter_by(id=row_id).first()
    db.session.delete(article)
    db.session.commit()
    return redirect(url_for('article'))

if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)