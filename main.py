from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

# 取得伺服器目前路徑
UPLOAD_FOLDER = os.getcwd() + os.path.sep + 'static/img'

# 檔案型態
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}

app = Flask(__name__)
app.secret_key = 'FlaskSE'
app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://DevAuth:Dev127336@127.0.0.1:3306/DevDb"

# 上傳路徑
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

# 限制大小 16MB
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

db = SQLAlchemy(app)
